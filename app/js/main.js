$(document).ready(function () {
    menu();
    
    function menu() {

        $('.hamburger').click(function (event) {
            event.preventDefault();
            $(this).toggleClass('active');
            $('.nav').toggleClass('active')
        });

        $(document).mouseup(function (event) {
            if ($(window).width() < 768) {
                event.preventDefault();
                var container = $('.header');
                if (!container.is(event.target) && container.has(event.target).length === 0) {
                    container.removeClass("active");
                    container.find(".hamburger").removeClass("active");
                    container.find(".nav").removeClass("active");
                }
            }
        });

    }

});