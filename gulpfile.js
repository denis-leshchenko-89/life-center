const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const notify = require('gulp-notify');
const del = require('del');
const browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');
const imageminPngquant = require('imagemin-pngquant');
const rigger = require('gulp-rigger');
const plumber = require('gulp-plumber');


gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
});

gulp.task('html', function () {
    return gulp.src('app/*.html')
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulp.dest('build'))
        .on('end', browserSync.reload)
})

gulp.task('sass', function () {
    return gulp.src('app/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded", }).on("error", notify.onError()))
        .pipe(autoprefixer({
            browsers: ['last 16 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('css', function () {
    return gulp.src('app/css/**/*.css')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: "expanded", }).on("error", notify.onError()))
        .pipe(autoprefixer({
            browsers: ['last 16 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('js', function () {
    return gulp.src('app/js/**/*.js')
        .pipe(plumber())
        .pipe(gulp.dest('build/js'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*.*')
        .pipe(plumber())
        .pipe(gulp.dest('build/fonts'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('images', function () {
    return gulp.src('app/images/**/*.*')
        .pipe(plumber())
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imageminJpegRecompress({
                progressive: true,
                min: 70,
                max: 80
            }),
            imageminPngquant({ quality: '80' }),
            imagemin.svgo({ plugins: [{ removeViewBox: true }] })
        ]))
        .pipe(gulp.dest('build/images'))
        .pipe(browserSync.reload({ stream: true }));
});


gulp.task('clean', function () {
    return del(["build"]);
})

gulp.task('watch', function () {
    gulp.watch('app/*.html', gulp.series('html'));
    gulp.watch('app/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('app/js/**/*.js', gulp.series('js'));
    gulp.watch('app/images/**/*.*', gulp.series('images'));
    gulp.watch('app/fonts/**/*.*', gulp.series('fonts'));

})

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('html', 'sass', 'css',  'js', 'images','fonts'),
))

gulp.task('default', gulp.series(
    gulp.parallel('html', 'sass', 'css', 'js', 'images','fonts'),
    gulp.parallel('watch', 'server')
))